Plus ou Moins (version 0.1)
---------------------------
Copyright (C) 2008, Naji Mammeri <naji.mammeri@gmail.com>.
Read license.txt for further information.

"Plus ou Moins" is a basic guess-the-number game written using PyGTK/Glade.
Even though the game in itself is absolutely useless, it might be considered as a tutorial for using PyGTK/Glade.

Installation
------------

"Plus ou Moins" is composed of a single Python file, and thus doesn't require any installation. Just make sure that the 'pygtk', 'gtk' and 'gtk.glade' modules for Python are installed on your system, then type in an interactive prompt:
$ python plusoumoins.py

Authors
-------

See authors.txt.

