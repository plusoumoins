#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, pygtk, random
pygtk.require('2.0')
import gtk, gtk.glade

GLADE_FILE = 'ui.glade'

class MainApp:
	game = {}
	
	def __init__(self):		
		# "About" popup
		self.about_dialog_tree = gtk.glade.XML(GLADE_FILE, 'about_dialog')
		self.about_dialog = self.about_dialog_tree.get_widget('about_dialog')
		callbacks = { "on_about_dialog_close":self._about_dialog_hide,
			"on_about_dialog_response":self._about_dialog_hide }
		self.about_dialog_tree.signal_autoconnect(callbacks)
		self.about_dialog.connect("delete-event", self.do_not_destroy)
		
		# "Success" popup
		self.success_dialog_tree = gtk.glade.XML(GLADE_FILE, 'success_dialog')
		self.success_dialog = self.success_dialog_tree.get_widget('success_dialog')
		callbacks = { "on_success_dialog_close":self._success_dialog_hide,
			"on_success_dialog_response":self._success_dialog_hide }
		self.success_dialog_tree.signal_autoconnect(callbacks)
		self.success_dialog.connect("delete-event", self.do_not_destroy)
		
		# Main window
		self.ui_tree = gtk.glade.XML(GLADE_FILE, 'main_window')
		callbacks = { "on_main_window_destroy":gtk.main_quit,
			"on_new_menu_entry_activate":self._new_game_callback,
			"on_quit_menu_entry_activate":gtk.main_quit,
			"on_about_menu_entry_activate":self._about_dialog_show,
			"on_number_button_released":self._validate_number }
		self.ui_tree.signal_autoconnect(callbacks)
		self.ui_tree.get_widget('main_window').show()
		
		self.spinbutton = self.ui_tree.get_widget('spinbutton')
		self._new_game()
	
	def run(self):
		gtk.main()
	
	## Accessors
	def user_estimation(self):
		return int(self.spinbutton.get_value())
	
	## Normal callbacks
	
	def _new_game_callback(self, widget, data=None): self._new_game()
	def _new_game(self):
		self.game['number_to_find'] = random.randint(0,5000)
		self.game['number_of_tries'] = 0
		self._update_help_label('Aucune tentative pour l\'instant')
	
	def _validate_number(self, widget, data=None):
		self.game['number_of_tries'] += 1
		if self.user_estimation() == self.game['number_to_find']:
			self.success_dialog.show()
			label_string = 'Yeah, bien joué !'
		elif self.user_estimation() > self.game['number_to_find']:
			label_string = 'Trop grand !'
		else:
			label_string = 'Trop petit !'
		label_string += "\n(Tentative %d)" % self.game['number_of_tries']
		self._update_help_label(label_string)
	
	def _update_help_label(self, string):
		self.ui_tree.get_widget('help_label').set_text(string)
	
	## Pop-ups
	
	# allows the popups to be displayed more than once
	def do_not_destroy(self, widget, event): return True
	
	def _success_dialog_hide(self, widget, data=None): self.success_dialog.hide()
	def _about_dialog_show(self, widget, data=None): self.about_dialog.show()
	def _about_dialog_hide(self, widget, data=None): self.about_dialog.hide()

if __name__ == '__main__':
	app = MainApp()
	app.run()
