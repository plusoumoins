from distutils.core import setup
setup(name='plusoumoins', version='0.1',
	description='Stupid guess-the-number game using PyGTK/Glade',
	author='Naji Mammeri', author_email='naji.mammeri@gmail.com',
	url='http://yemsel.free.fr/plusoumoins/',
	
	py_modules=['plusoumoins'],
	requires=['pygtk', 'gtk', 'gtk.glade'],
	package_data={'':['ui.glade']},
)

